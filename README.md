# Note to maintainers

So, you've added one or more of your projects to the Code Shelter, great! In
order for Code Shelter maintainers to know what sort of help you need, we'd
appreciate it if you could download the [CODESHELTER.md](CODESHELTER.md) file in
this repository, address the questions in it and commit it to your project's
repository.

You can download the file [by clicking
here](https://gitlab.com/codeshelter/note-to-maintainers/raw/master/CODESHELTER.md?inline=false)
 or by running:

```
curl "https://gitlab.com/codeshelter/note-to-maintainers/raw/master/CODESHELTER.md" > CODESHELTER.md
```

Also, don't forget to join our [chat server](https://codeshelter.zulipchat.com/)
as that's where most of the discussion happens. We'll see you there!
