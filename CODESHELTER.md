﻿# Note to Code Shelter maintainers

This file can be used as a note to all Code Shelter maintainers. If this note is
not edited, or is missing from your repository, Code Shelter maintainers will do
their best to make good choices for this project. If, however, you want to give a little
direction, please fill out.

Things you may want to address:

- What abilities and involvement do you wish to maintain? Do you just want
  to improve the project's bus factor, do you want issue and pull request
  management, or are you stepping away entirely? The less ambiguity you have on
  this point, the more confident new maintainers will be that they are not
  overstepping - which means they and the project will be more active.
- Are you giving publishing permissions to Code Shelter maintainers? (PyPI, npm,
  etc)
- What kind of help are you hoping for?
- Are there additional resources you need to give access to manually?
- Do you have any helpful knowledge about this codebase you'd like to share
  here? Consider non-obvious technical debt, hurdles, and paths forward.
- Are there any ongoing considerations you would like new maintainers to be
  aware of?


## Example note

Here's a good note, as an example. Keep in mind that you should delete
everything else and just keep your text:

---

# Note to Code Shelter maintainers

I don't have as much time as I'd like to dedicate to this project these days,
but many people find it useful, and I'd appreciate some help with maintaining
it. I will generally be available to consult and maybe develop the occasional
feature or bugfix, but my availability is not reliable.

It would be great if you could help out with issue triage, fixing bugs and
occasionally developing new features that you think are necessary. The project
is mainly feature-complete, so I don't expect any major deviation and would
appreciate being consulted before making any drastic changes, but other than
that, you have free reign.

I have already added Code Shelter to the project's PyPI page, so feel free to
make any releases necessary. The codebase is in reasonable shape, although there
is a legacy component that is hard to maintain and which lives in the `foo`
directory.

Thank you!